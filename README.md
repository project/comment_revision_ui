# Comment Revision UI

This module takes advantage of work in progress Drupal core patches to add a 
revision UI for comments. Most of the code of this project was taken from the 
Block Content Revision UI module.

## Instructions
1. Apply the work from 
   [#2880154: Convert comments to be revisionable](https://www.drupal.org/project/drupal/issues/2880154)
   as patch to your project to make comment entities revisionable.
2. Run database updates
3. Apply the work from [#2350939: Implement a generic revision UI](https://www.drupal.org/project/drupal/issues/2350939)
   as patch to your project to include the generic revision UI which is used by 
   this module.
4. Assign the appropriate user permissions. See 'Comment Revision UI' section in
   `/admin/people/permissions`.
5. Export any configuration changes if necessary
