<?php

namespace Drupal\comment_revision_ui;

use Drupal\comment\CommentTypeInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds revision permissions for comment.
 */
class CommentRevisionUiPermissions implements ContainerInjectionInterface {

  /**
   * Comment type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $commentTypeStorage;

  /**
   * CommentRevisionUiPermissions constructor.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $commentTypeStorage
   *   Comment type storage.
   */
  public function __construct(EntityStorageInterface $commentTypeStorage) {
    $this->commentTypeStorage = $commentTypeStorage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('comment_type')
    );
  }

  /**
   * Generate dynamic permissions.
   */
  public function permissions() {
    $permissions = [];

    foreach ($this->commentTypeStorage->loadMultiple() as $bundle => $commentType) {
      assert($commentType instanceof CommentTypeInterface);
      $bundleLabel = $commentType->label();
      $permissions['view comment ' . $bundle . ' history'] = [
        'title' => 'View ' . $bundleLabel . ' history pages',
      ];
      $permissions['view comment ' . $bundle . ' revisions'] = [
        'title' => 'View ' . $bundleLabel . ' revisions pages',
      ];
      $permissions['revert comment ' . $bundle . ' revisions'] = [
        'title' => 'Revert ' . $bundleLabel . ' revisions',
      ];
      $permissions['delete comment ' . $bundle . ' revisions'] = [
        'title' => 'Delete ' . $bundleLabel . ' revisions',
      ];
    }

    return $permissions;
  }

}
