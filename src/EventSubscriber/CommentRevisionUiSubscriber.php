<?php

namespace Drupal\comment_revision_ui\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Make the comment revision routes admin routes.
 */
class CommentRevisionUiSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $adminRoutes = [
      'entity.comment.version_history',
      'entity.comment.revision',
      'entity.comment.revision_revert_form',
      'entity.comment.revision_delete_form',
    ];

    foreach ($adminRoutes as $adminRoute) {
      $revisionDeleteFormRoute = $collection->get($adminRoute);
      $revisionDeleteFormRoute->setOption('_admin_route', TRUE);
    }
  }

}
